const report = require('./report.js').report;

// function tasker(name, runner){
//     if(!(this instanceof tasker))return new tasker(name, runner);
//     this.name = "'" + name + "'";
//     this.runner = runner;
// }
// tasker.prototype = {
//     run: function(times){
//         const string = this.name + (times ? " times: " + times : "");
//         report("Starting " + string);
//         return new Promise((resolve, reject) => {
//             return this.runner(data => {
//                 report("Finished " + string);
//                 resolve(data);
//             }, reason => {
//                 report("Rejected " + string, 'error');
//                 reject(reason);
//             });
//         });
//     }
// };
let taskTimes = {};
function tasker(name, runner){
    if(!(name in taskTimes))taskTimes[name] = 0;
    let times = taskTimes[name]++;
    let string = "'" + name + "'" + (times ? " times: " + times : "");
    report("Starting " + string);
    return new Promise((resolve, reject) => {
        return runner(data => {
            report("Finished " + string);
            resolve(data);
        }, reason => {
            report("Rejected " + string, 'error');
            reject(reason);
        });
    });
}
tasker.clear = function(){
    taskTimes = {};
};

module.exports = tasker;

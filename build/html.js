const log = require('./report.js').log;
const gulp = require('gulp');
// const glob = require('glob');
const path = require('path');
const replace = require('gulp-replace');
const tasker = require('./tasker.js');

// all task
const task = {};
module.exports = task;

task.build = function(env, revJson){
    return tasker('Build Html', (resolve, reject) => {
        moveFiles()
            .then(() => buildHtml(env, revJson), reject)
            .then(resolve, reject);
    });
};

// build html
function buildHtml(env, rev){
    return new Promise(function(resolve, reject){
        gulp.src([path.resolve("static/index.html")])
            .pipe(replace(/#if#(dev|pro)#((?:.|\n)*?)#endif#/g, (match, $1, $2) => $1 === env ? $2 : ''))
            .pipe(replace(/#rev#version#/g, match => JSON.stringify(rev)))
            .pipe(replace(/#rev#(.+?)#/g, (match, $1) => rev[$1]))
            .pipe(gulp.dest(path.resolve("./dist")))
            .on("end", (...args) => {
                resolve(args);
            });
    });
}

// copy src rest files
function moveFiles(){
    return new Promise(function(resolve, reject){
        gulp.src([path.resolve("./static/**"), '!' + path.resolve("./static/index.html")])
            .pipe(gulp.dest(path.resolve("./dist")))
            .on("end", (...args) => {
                resolve(args);
            });
    });
}

task.watch = function(revJson, callback){
    gulp.watch([path.resolve('./static/**/*')], function(ev){
        log("File [..." + ev.path.slice(-36) + "] changes. Building......");
        task.build('dev', revJson).then(...callback);
    });
};

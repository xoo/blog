const colors = require('colors');
const moment = require('moment');

// config
const reports = require('./config/reports.json');

// set colors theme
colors.setTheme({
    silly: 'rainbow',
    // input: 'grey',
    // verbose: 'cyan',
    // prompt: 'red',
    info: 'green',
    data: 'blue',
    help: 'cyan',
    warn: 'yellow',
    debug: 'magenta',
    error: 'red'
});

const API = {
    report: function(info, theme = false){
        console.log('[' + moment().format('HH:mm:ss') + '] ' + (theme ? colors[theme](info) : info));
    },
    log: function(info, theme = false, extra = ''){
        console.log((theme ? colors[theme](info) : info) + extra);
    },
    trigger: function(order){
        API.log(reports[order].msg, reports[order].theme || false);
    }
};

module.exports = API;

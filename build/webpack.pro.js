const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    mode: 'production',
    output: {
        filename: "[name]-[chunkhash:10].js",
        chunkFilename: "[name]-[chunkhash:10].js"
    },
    plugins: [
        new webpack.HashedModuleIdsPlugin(),
        new webpack.DefinePlugin({
            'process.env': { NODE_ENV: '"production"' }
        }),
        new UglifyJsPlugin({
            cache: true,
            uglifyOptions: {
                ecma: 5,
                compress: { warnings: false },
            },
            // minimize: true,
            // output: { comments: false, },

        })
    ]
};
